package galileo;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.lang.JoseException;

import java.util.List;

public class JwtUserType extends JwtGalileo {


    public JwtUserType(String username, String email,List<String> permissions) {
        super(ReservedGalileoNames.USER_TYPE);
        this.setUsername(username);
        this.setEmail(email);
        this.setPermissions(permissions);
    }


    public String createJWS(JwtUserType claims, RsaJsonWebKey rsaJsonWebKey) throws JoseException {
        return super.createJWS(rsaJsonWebKey);
    }


    public void setUsername(String username)
    {
        setStringClaim(ReservedGalileoNames.USERNAME, username);
    }

    public String getUsername() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.USERNAME, String.class);
    }

    public void setEmail(String email){
        setStringClaim("email",email);
    }
    public String getEmailValue() throws MalformedClaimException
    {
        return getStringClaimValue("email");
    }


    public void setPermissions(List permissions){
        setStringListClaim("permissions",permissions);
    }
    public List<String> getPermissionsValue() throws MalformedClaimException
    {
        return getStringListClaimValue("permissions");
    }
}
