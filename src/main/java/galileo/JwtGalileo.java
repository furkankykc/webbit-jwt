package galileo;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.lang.JoseException;

public class JwtGalileo extends JwtClaims {
    private String username;
    private String password;
    private String authToken;
    private String requestField;
    private String responseField;
    private String type;

    public JwtGalileo(String type) {
        this.type = type;
        super.setIssuer("Issuer");  // who creates the token and signs it
        super.setExpirationTimeMinutesInTheFuture(10); // time when the token will expire (10 minutes from now)
        super.setSubject(ReservedGalileoNames.TYPE);
        super.setClaim("type",type);
        super.setGeneratedJwtId(); // a unique identifier for the token
        super.setIssuedAtToNow();  // when the token was issued/created (now)
        super.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
        //claims.setSubject("exampleJwt"); // the subject/principal is whom the token is about
        super.setClaim("email", "mail@example.com"); // additional claims/attributes about the subject can be added

    }

    public void setType(String type)
    {
        setStringClaim(ReservedGalileoNames.TYPE, type);
    }

    public String getType() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.TYPE, String.class);
    }

    public String createJWS(RsaJsonWebKey rsaJsonWebKey) throws JoseException {
        JsonWebSignature jws = new JsonWebSignature();

        // The payload of the JWS is JSON content of the JWT Claims
        jws.setPayload(this.toJson());

        // The JWT is signed using the private key
        jws.setKey(rsaJsonWebKey.getPrivateKey());

        // Set the Key ID (kid) header because it's just the polite thing to do.
        // We only have one key in this example but a using a Key ID helps
        // facilitate a smooth key rollover process
        jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());

        // Set the signature algorithm on the JWT/JWS that will integrity protect the claims
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);

        // Sign the JWS and produce the compact serialization or the complete JWT/JWS
        // representation, which is a string consisting of three dot ('.') separated
        // base64url-encoded parts in the form Header.Payload.Signature
        // If you wanted to encrypt it, you can simply set this jwt as the payload
        // of a JsonWebEncryption object and set the cty (Content Type) header to "jwt".
        String jwt = jws.getCompactSerialization();
        return jwt;
    }
}
