package galileo;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.lang.JoseException;

public class JwtResponseType extends JwtGalileo{



        public JwtResponseType(String authToken,String responseField) {
            super(ReservedGalileoNames.RESPONSE_TYPE);
            this.setAuthToken(authToken);
            this.setResponseField(responseField);
        }

    public String createJWS(RsaJsonWebKey rsaJsonWebKey) throws JoseException {
        return super.createJWS(rsaJsonWebKey);
    }

    public void setAuthToken(String authToken)
    {
        setStringClaim(ReservedGalileoNames.AUTHTOKEN, authToken);
    }

    public String getAuthToken() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.AUTHTOKEN, String.class);
    }
    public String getResponseField() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.RESPONSEFIELD, String.class);
    }

    public void setResponseField(String responseField)
    {
        setStringClaim(ReservedGalileoNames.RESPONSEFIELD, responseField);
    }

    }


