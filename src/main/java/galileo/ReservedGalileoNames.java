/*
 * Copyright 2012-2017 Brian Campbell
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package galileo;

/**
 */
public class ReservedGalileoNames
{
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String AUTHTOKEN = "auth_token";
    public static final String REQUESTFIELD = "auth_token";
    public static final String RESPONSEFIELD = "response_field";
    public static final String AUTH_REQUEST_TYPE = "AUTH_REQUEST_TYPE";
    public static final String MSG_REQUEST_TYPE = "MSG_REQUEST_TYPE";
    public static final String RESPONSE_TYPE = "RESPONSE_TYPE";
    public static final String USER_TYPE = "USER_TYPE";
    /**
     * @deprecated typ went away as a claim name as of jwt draft -12 - it's only a header parameter name
     */
    public static final String TYPE = "galileoJsonObject";
}