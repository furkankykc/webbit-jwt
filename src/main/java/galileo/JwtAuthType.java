package galileo;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.lang.JoseException;

public class JwtAuthType extends JwtGalileo {



    public JwtAuthType(String username,String password) {
        super(ReservedGalileoNames.AUTH_REQUEST_TYPE);
        this.setUsername(username);
        this.setPassword(password);
    }


    public String createJWS(JwtAuthType claims, RsaJsonWebKey rsaJsonWebKey) throws JoseException {
        return super.createJWS(rsaJsonWebKey);
    }

    public void setUsername(String username)
    {
        setStringClaim(ReservedGalileoNames.USERNAME, username);
    }

    public String getUsername() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.USERNAME, String.class);
    }
    public void setPassword(String password)
    {
        setStringClaim(ReservedGalileoNames.PASSWORD, password);
    }

    public String getPassword() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.PASSWORD, String.class);
    }



}
