package galileo;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

public class JwtGalileoBuilder extends JwtConsumerBuilder {

    private RsaJsonWebKey rsaJsonWebKey;
    private String expectedSubject;
    public JwtGalileoBuilder(RsaJsonWebKey rsaJsonWebKey) {
        this.expectedSubject = "galileoJsonObject";
        this.rsaJsonWebKey = rsaJsonWebKey;

    }

    @Override
    public JwtConsumer build() {

        return new JwtConsumerBuilder()
                .setRequireExpirationTime() // the JWT must have an expiration time
                .setAllowedClockSkewInSeconds(30) // allow some leeway in validating time based claims to account for clock skew
                .setRequireSubject() // the JWT must have a subject claim
                .setExpectedSubject(expectedSubject)
                .setVerificationKey(rsaJsonWebKey.getKey()) // verify the signature with the public key
                .setJwsAlgorithmConstraints( // only allow the expected signature algorithm(s) in the given context
                        new AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST, // which is only RS256 here
                                AlgorithmIdentifiers.RSA_USING_SHA256)).build();

    }
}
