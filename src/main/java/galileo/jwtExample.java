package galileo;


import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.ErrorCodes;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.lang.JoseException;

import java.util.Arrays;
import java.util.List;
public class jwtExample {


    public static void main(String args[]) throws JoseException,MalformedClaimException {


        // Now you can do something with the JWT. Like send it to some other party
        // over the clouds and through the interwebs.
        RsaJsonWebKey rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
        // Give the JWK a Key ID (kid), which is just the polite thing to do
        rsaJsonWebKey.setKeyId("k1");

        String jwt = new JwtAuthType("root","1234").createJWS(rsaJsonWebKey);
        System.out.println("JWT: " + jwt);


        // Use JwtConsumerBuilder to construct an appropriate JwtConsumer, which will
        // be used to validate and process the JWT.
        // The specific validation requirements for a JWT are context dependent, however,
        // it typically advisable to require a (reasonable) expiration time, a trusted issuer, and
        // and audience that identifies your system as the intended recipient.
        // If the JWT is encrypted too, you need only provide a decryption key or
        // decryption key resolver to the builder.




        JwtConsumer jwtAuth = new JwtGalileoBuilder(rsaJsonWebKey).build(); // create the JwtConsumer instance

        try {
            //  Validate the JWT and process it to the Claims
            JwtClaims jwtClaims = jwtAuth.processToClaims(jwt);
            System.out.println("JWT validation succeeded! " + jwtClaims.getSubject());
            if(jwtClaims.getClaimValue("type").equals(ReservedGalileoNames.AUTH_REQUEST_TYPE)){
                    /*login (
                    jwtClaims.getClaimValue("username"),
                    jwtClaims.getClaimValue("password"));*/
                    List<String> permissions = Arrays.asList("permission-one", "permission-two", "permission-three");
                    JwtGalileo exJWT = new JwtUserType("","",permissions);
                    System.out.println("Payload : "+ exJWT.createJWS(rsaJsonWebKey));

            }else if(jwtClaims.getClaimValue("type").equals(ReservedGalileoNames.RESPONSE_TYPE)){

            }else if(jwtClaims.getClaimValue("type").equals(ReservedGalileoNames.MSG_REQUEST_TYPE)){

            }


            System.out.println(jwtClaims.getClaimValue("username"));
        } catch (InvalidJwtException e) {
            // InvalidJwtException will be thrown, if the JWT failed processing or validation in anyway.
            // Hopefully with meaningful explanations(s) about what went wrong.
            System.out.println("Invalid JWT! " + e);

            // Programmatic access to (some) specific reasons for JWT invalidity is also possible
            // should you want different error handling behavior for certain conditions.

            // Whether or not the JWT has expired being one common reason for invalidity
            try {
                if (e.hasExpired()) {

                    System.out.println("JWT expired at " + e.getJwtContext().getJwtClaims().getExpirationTime());
                }

                // Or maybe the audience was invalid
                if (e.hasErrorCode(ErrorCodes.AUDIENCE_INVALID)) {
                    System.out.println("JWT had wrong audience: " + e.getJwtContext().getJwtClaims().getAudience());
                }

                }catch (MalformedClaimException ex){
                     ex.printStackTrace();
                     }


        }
    }
}
