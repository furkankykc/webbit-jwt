package galileo;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.lang.JoseException;

public class JwtRequestType extends JwtGalileo{



        public JwtRequestType(String authToken, String requestField) {
            super(ReservedGalileoNames.MSG_REQUEST_TYPE);
            this.setAuthToken(authToken);
            this.setRequestField(requestField);
        }

    public void setAuthToken(String authToken)
    {
        setStringClaim(ReservedGalileoNames.AUTHTOKEN, authToken);
    }

    public String getAuthToken() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.AUTHTOKEN, String.class);
    }

    public void setRequestField(String requestField)
    {
        setStringClaim(ReservedGalileoNames.REQUESTFIELD, requestField);
    }

    public String getRequestField() throws MalformedClaimException
    {
        return getClaimValue(ReservedGalileoNames.REQUESTFIELD, String.class);
    }


    public String createJWS(RsaJsonWebKey rsaJsonWebKey) throws JoseException {
        return super.createJWS(rsaJsonWebKey);
    }
    }


